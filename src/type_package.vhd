-------------------------------------------------------------------------------
-- Title      : MIPS-X
-- Project    :
-------------------------------------------------------------------------------
-- File       : type_package.vhd
-- Author     : Jean-Christophe Le Lann  lelannje@ensta-bretagne.fr
-- Company    :
-- Created    : 2014-03-25
-- Last update: 2018-03-23
-- Platform   :
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-03-25  1.0      jcll    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package type_package is

  constant CODE_ADDR_WIDTH : natural := 8;
  constant DATA_ADDR_WIDTH : natural := 8;

  constant DEFAULT_BOOT_ADDRESS : unsigned(CODE_ADDR_WIDTH-1 downto 0) := to_unsigned(0, CODE_ADDR_WIDTH);

  -----------------------------------------------------------------------------
  -- Opcodes
  -----------------------------------------------------------------------------
  subtype opcode is std_logic_vector(4 downto 0);

  constant ADD_OP   : opcode := "00001";
  constant SUB_OP   : opcode := "00010";  --ghdl bug when 'SUB' only...
  constant MUL_OP   : opcode := "00011";
  constant DIV_OP   : opcode := "00100";
  constant AND_OP   : opcode := "00101";
  constant OR_OP    : opcode := "00110";
  constant XOR_OP   : opcode := "00111";
  constant SHL_OP   : opcode := "01000";
  constant SHR_OP   : opcode := "01001";
  constant SLT_OP   : opcode := "01010";
  constant SLE_OP   : opcode := "01011";
  constant SEQ_OP   : opcode := "01100";
  constant LOAD_OP  : opcode := "01101";
  constant STORE_OP : opcode := "01110";
  constant JMP_OP   : opcode := "01111";
  constant BRAZ_OP  : opcode := "10000";
  constant BRANZ_OP : opcode := "10001";
  constant SCALL_OP : opcode := "10010";
  constant STOP_OP  : opcode := "00000";
  -----------------------------------------------------------------------------
  --
  -----------------------------------------------------------------------------
  type codeop is (NOP, ADD, SUB, MUL, DIV, AND_C, OR_C, XOR_C, SHL_C, SHR_C, SLT_C, SLE_C,
                  SEQ_C, LOAD, STORE, JMP, BRANZ, BRAZ, SCALL, STOP);
  -----------------------------------------------------------------------------
  -- Running modes
  -----------------------------------------------------------------------------
  subtype running_mode is std_logic_vector(2 downto 0);

  constant STILL_MODE     : running_mode := "000";
  constant PROG_MODE      : running_mode := "001";
  constant DATA_MODE      : running_mode := "010";
  constant BOOT_MODE      : running_mode := "011";
  constant RUN_MODE       : running_mode := "100";
  constant DUMP_PROG_MODE : running_mode := "101";
  constant DUMP_DATA_MODE : running_mode := "110";


  -----------------------------------------------------------------------------
  --  Memory/data types
  -----------------------------------------------------------------------------
  constant MAX_SIZE : natural := 2**CODE_ADDR_WIDTH-1;
  subtype data_type is std_logic_vector(31 downto 0);
  type memory_type is array(0 to MAX_SIZE) of data_type;



end package;
