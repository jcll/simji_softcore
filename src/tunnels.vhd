library ieee;
use ieee.std_logic_1164.all;

package tunnels is
  signal tunnel_r1 : std_logic_vector(31 downto 0);
end tunnels;
