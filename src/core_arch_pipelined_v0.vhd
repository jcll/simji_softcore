-------------------------------------------------------------------------------
-- Title      : Simple (slow) RISC 32 processor with no pipeline
-- Project    : MIPS0
-------------------------------------------------------------------------------
-- File       : core.vhd
-- Author     : Jean-Christophe Le Lann  lelannje@ensta-bretagne.fr
-- Company    :
-- Created    : 2014-03-05
-- Last update: 2019-04-05
-- Platform   :
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-03-05  1.0      jcll    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library simji_core;
use simji_core.type_package.all;

library misc_lib;
use misc_lib.txt_util.all;
use misc_lib.tunnels.all;

architecture archi_v0 of core is

  subtype reg_type is std_logic_vector(31 downto 0);
  type reg_bank is array(0 to 31) of reg_type;
  signal regs_r, regs_c : reg_bank;

  type state_t is (idle, decode_execute, execute_load);
  signal state_r, state_c : state_t;

  signal code_we_r, code_we_c           : std_logic;
  signal code_sel_r, code_sel_c         : std_logic;
  --signal code_address_r, code_address_c : unsigned(CODE_ADDR_WIDTH-1 downto 0);
  signal pc_r,pc_c                      : unsigned(CODE_ADDR_WIDTH-1 downto 0);
  signal instr_r, instr_c               : std_logic_vector(31 downto 0);

  signal data_we_r, data_we_c           : std_logic;
  signal data_sel_r, data_sel_c         : std_logic;
  signal data_address_r, data_address_c : unsigned(DATA_ADDR_WIDTH-1 downto 0);
  signal data_out_c                     : std_logic_vector(31 downto 0);
  signal stopped_r, stopped_c           : std_logic;
  signal running_r,running_c            : std_logic;
  signal leds_r,leds_c                  : std_logic_vector(15 downto 0);
  -- synthesis off
  signal codop                          : codeop;
-- synthesis on
  signal go_r : std_logic;
begin  -- rtl

  -----------------------------------------------------------------------------
  -- initial sequencing. Need to be enhanced!
  -----------------------------------------------------------------------------
  seq : process (clk, reset_n)
  begin
    if reset_n = '0' then
      state_r        <= idle;

      pc_r           <= DEFAULT_BOOT_ADDRESS;
      code_we_r      <= '0';
      code_sel_r     <= '0';

      data_address_r <= (others=>'0');
      data_sel_r     <= '0';
      data_we_r      <= '0';

      instr_r        <= (others => '0');
      regs_r         <= (others => (others => '0'));
      regs_r(1)      <= x"00000005";
      stopped_r      <= '0';
      running_r      <= '0';
      leds_r         <= (others=>'0');
      go_r           <= '0';
    elsif rising_edge(clk) then
      state_r        <= state_c;
      go_r           <= go;
      pc_r           <= pc_c;
      code_we_r      <= code_we_c;
      code_sel_r     <= code_sel_c;

      data_address_r <= data_address_c;
      data_sel_r     <= data_sel_c;
      data_we_r      <= data_we_c;

      instr_r        <= instr_c;
      regs_r         <= regs_c;
      stopped_r      <= stopped_c;
      running_r      <= running_c;
      leds_r         <= leds_c;
    end if;
  end process;

  stopped <= stopped_r;
  leds    <= leds_r;
  -----------------------------------------------------------------------------
  code_address <= pc_c;
  code_sel     <= code_sel_c;
  code_we      <= code_we_c;

  -------------------------------------------------------------------------------
  ---- Decode + execute
  -------------------------------------------------------------------------------
  comb : process (go_r,running_r, code_in, data_address_r, instr_r, data_in, pc_r,
                  regs_r, state_r, stopped_r,pc_r,running_r,stopped_r,leds_r)
    variable state_v        : state_t;
    variable running_v       : std_logic;
    variable pc_v           : unsigned(CODE_ADDR_WIDTH-1 downto 0);
    variable code_we_v      : std_logic;
    variable code_sel_v     : std_logic;
    variable stopped_v      : std_logic;
    variable data_we_v      : std_logic;
    variable data_sel_v     : std_logic;
    variable data_address_v : unsigned(DATA_ADDR_WIDTH-1 downto 0);
    variable data_out_v     : std_logic_vector(31 downto 0);
    variable instr_v        : std_logic_vector(31 downto 0);
    variable nr1, nr2       : integer range 0 to 31;
    variable o, old_o       : signed(31 downto 0);
    variable regs_v         : reg_bank;
    variable opcode_v       : opcode;
    variable o_is_imm_v     : std_logic;
    variable o_is_reg_str   : string(1 to 1);
    variable leds_v         : std_logic_vector(15 downto 0);
    variable scall_number_v : unsigned(26 downto 0);
  begin
    ---------------------------------------------------------------------------
    -- default assignements
    ---------------------------------------------------------------------------
    state_v        := state_r;
    pc_v           := pc_r;
    code_we_v      := '0';              --?
    code_sel_v     := '0';

    data_we_v      := '0';
    data_sel_v     := '0';
    data_address_v := data_address_r;   --?
    data_out_v     := (others => '0');
    regs_v         := regs_r;
    instr_v        := instr_r;          --(others => '0');
    data_address_v := (others => '0');
    running_v      := running_r;
    stopped_v      := stopped_r;
    running_v      := running_r;
    leds_v         := leds_r;
    scall_number_v := (others=>'0');

    case state_v is

      when IDLE =>
        if go_r = '1' then
          running_v      := '1';
          stopped_v      := '0';
          code_sel_v     := '1';
          code_we_v      := '0';
          pc_v           := DEFAULT_BOOT_ADDRESS;
          state_v        := DECODE_EXECUTE;
        end if;

      when DECODE_EXECUTE =>
        -- FETCH :
        pc_v := pc_v+1;
        code_sel_v := '1';
        code_we_v  := '0';
        --
        instr_v  := code_in;
        opcode_v := instr_v(31 downto 27);
        nr2      := to_integer(unsigned(instr_v(4 downto 0)));
        nr1      := to_integer(unsigned(instr_v(26 downto 22)));

        o          := resize(signed(instr_v(20 downto 5)), 32);
        o_is_imm_v := instr_v(21);
        scall_number_v := unsigned(instr_v(26 downto 0));
        --synthesis off
        if o_is_imm_v = '1' then
          o_is_reg_str := " ";
        else
          o_is_reg_str := "r";
        end if;
        --synthesis on

        old_o := o;
        if o_is_imm_v = '0' then
          o := signed(regs_v(to_integer(unsigned(o))));
        end if;


        case opcode_v is

          when ADD_OP =>
          --synthesis off
            report "ADD r" & integer'image(nr1) & " " & o_is_reg_str & integer'image(to_integer(old_o)) & " r" & integer'image(nr2);
            codop       <= ADD;
            -- synthesis on
            regs_v(nr2) := std_logic_vector(signed(regs_v(nr1)) + o);

          when SUB_OP =>
          --synthesis off
            codop       <= SUB;
            report "SUB r" & integer'image(nr1) & " " & o_is_reg_str & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := std_logic_vector(signed(regs_v(nr1)) - o);

          when MUL_OP =>
          --synthesis off
            codop       <= MUL;
            report "MUL r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := std_logic_vector(resize(signed(regs_v(nr1)) * o, 32));

          when DIV_OP =>
          --synthesis off
            codop       <= DIV;
            report "DIV r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            -- WARNING !!!!!! NIY
            --regs_v(nr2) := std_logic_vector(signed(regs_v(nr1)) + o);

          when AND_OP =>
          --synthesis off
            codop       <= AND_C;
            report "AND r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := regs_v(nr1) and std_logic_vector(o);

          when OR_OP =>
          --synthesis off
            codop       <= OR_C;
            report "OR r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := regs_v(nr1) or std_logic_vector(o);

          when XOR_OP =>
          --synthesis off
            codop       <= XOR_C;
            report "XOR r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := regs_v(nr1) xor std_logic_vector(o);

          when SHL_OP =>
          --synthesis off
            codop       <= SHL_C;
            report "SHL r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := std_logic_vector(shift_left(signed(regs_v(nr1)), to_integer(o)));

          when SHR_OP =>
          --synthesis off
            codop       <= SHR_C;
            report "SHR r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            regs_v(nr2) := std_logic_vector(shift_right(signed(regs_v(nr1)), to_integer(o)));

          when SLT_OP =>
          --synthesis off
            codop <= SLT_C;
            report "SLT r" & integer'image(nr1) & " " & o_is_reg_str & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            if signed(regs_v(nr1)) < o then
              regs_v(nr2) := std_logic_vector(to_unsigned(1, 32));
            else
              regs_v(nr2) := std_logic_vector(to_unsigned(0, 32));
            end if;

          when SLE_OP =>
          --synthesis off
            codop                  <= SLE_C;
            report "SLE r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(old_o)) & " r" & integer'image(nr2);
            -- synthesis on
            if signed(regs_v(nr1)) <= o then
              regs_v(nr2) := std_logic_vector(to_unsigned(1, 32));
            else
              regs_v(nr2) := std_logic_vector(to_unsigned(0, 32));
            end if;

          when SEQ_OP =>
          --synthesis off
            codop <= SEQ_C;
            report "SEQ r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            if signed(regs_v(nr1)) = o then
              regs_v(nr2) := std_logic_vector(to_unsigned(1, 32));
            else
              regs_v(nr2) := std_logic_vector(to_unsigned(0, 32));
            end if;

          when LOAD_OP =>
            --synthesis off
            codop          <= LOAD;
            report "LOAD r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            data_sel_v     := '1';
            data_we_v      := '0';
            data_address_v := resize(unsigned(resize(signed(regs_v(nr1)), 16) + o), DATA_ADDR_WIDTH);
            state_v        := execute_load;

          when STORE_OP =>
          --synthesis off
            codop          <= STORE;
            report "STORE r" & integer'image(nr1) & " " & o_is_reg_str & " " & " " & integer'image(to_integer(o)) & " r" & integer'image(nr2);
            -- synthesis on
            data_sel_v     := '1';
            data_we_v      := '1';
            data_address_v := resize(unsigned(resize(signed(regs_v(nr1)), 16) + o), DATA_ADDR_WIDTH);
            data_out_v     := regs_v(nr2);
            state_v        := decode_execute;

          when JMP_OP =>
            o          := resize(signed(instr_v(25 downto 5)), 32);
            o_is_imm_v := instr_v(26);
            --synthesis off
            if o_is_imm_v = '1' then
              o_is_reg_str := " ";
            else
              o_is_reg_str := "r";
            end if;
            -- synthesis on
            old_o := o;
            if o_is_imm_v = '0' then
              o := signed(regs_v(to_integer(unsigned(o))));
            end if;
            --synthesis off
            codop          <= JMP;
            report "JMP " & o_is_reg_str & " " & integer'image(to_integer(old_o)) & " r" & integer'image(nr2);
            -- synthesis on
            code_sel_v     := '1';
            code_we_v      := '0';
            regs_v(nr2)    := std_logic_vector(resize(pc_v, 32));
            pc_v           := resize(unsigned(o), CODE_ADDR_WIDTH);
            state_v        := decode_execute;

          when BRAZ_OP =>
            o := resize(signed(instr_v(21 downto 0)), 32);
            if regs_v(nr1) = x"00000000" then
              code_sel_v     := '1';
              code_we_v      := '0';
              pc_v           := resize(unsigned(o), CODE_ADDR_WIDTH);
              state_v        := decode_execute;
            end if;
            --synthesis off
            codop <= BRAZ;
            report "BRAZ r" & integer'image(nr1) & " " & integer'image(to_integer(o));
            -- synthesis on

          when BRANZ_OP =>
            o     := resize(signed(instr_v(21 downto 0)), 32);
            if regs_v(nr1) /= x"00000000" then
              code_sel_v     := '1';
              code_we_v      := '0';
              pc_v           := resize(unsigned(o), CODE_ADDR_WIDTH);
              state_v        := decode_execute;
            end if;
            --synthesis off
              codop <= BRANZ;
              report "BRANZ r" & integer'image(nr1) & " " & integer'image(to_integer(o));
            -- synthesis on

          when SCALL_OP =>
          --synthesis off
            codop <= SCALL;
            report "SCALL " & integer'image(to_integer(scall_number_v));
            -- synthesis on
            if unsigned(scall_number_v)=1 then
              leds_v := regs_v(1)(15 downto 0);
            end if;

          when STOP_OP =>
          --synthesis off
            codop     <= STOP;
            report "STOP ";
            --synthesis on
            running_v := '0';
            stopped_v := '1';
            state_v   := idle;

          when others => null;

        end case;

      when execute_load =>
        regs_v(nr2) := data_in;
        state_v     := DECODE_EXECUTE;

      when others => null;

    end case;

    ---------------------------------------------------------------------------
    -- Final assignement to signal of all variables
    ---------------------------------------------------------------------------
    regs_v(0) := (others => '0');

    regs_c  <= regs_v;
    state_c   <= state_v;

    stopped_c <= stopped_v;
    running_c <= running_v;

    pc_c      <= pc_v;
    code_we_c      <= code_we_v;
    code_sel_c     <= code_sel_v;

    data_we_c      <= data_we_v;
    data_sel_c     <= data_sel_v;
    data_address_c <= data_address_v;
    data_out_c     <= data_out_v;

    instr_c <= instr_v;
    leds_c  <= leds_v;

  end process;

  -----------------------------------------------------------------------------
  -- Comb(!) outputs
  -----------------------------------------------------------------------------
  data_out     <= data_out_c;
  data_address <= data_address_c;
  data_we      <= data_we_c;
  data_sel     <= data_sel_c;
  -----------------------------------------------------------------------------

  -- synthesis off
  tunnel_r1 <= regs_r(1);
  -- synthesis on
end archi_v0;
