library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
  port(
    clk100mhz : in std_logic
    );
end entity;

architecture rtl of top is
begin

  
  
  
  spm_core: entity work.system(rtl)
    port map (
      reset_n       => reset_n,
      clk           => clk,
      go            => go,
      mode          => mode,
      prog_addr     => prog_addr,
      prog_datain   => prog_datain,
      prog_dataoout => prog_dataoout,
      stopped       => stopped);


end rtl;
