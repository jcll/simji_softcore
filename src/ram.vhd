-------------------------------------------------------------------------------
-- Simple port ram (synthesizable as blockram)
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram is
  generic(ADDR_WIDTH : natural := 8;
          DATA_WIDTH : natural := 32);
  port (
    reset_n : in std_logic;
    clk     : in std_logic;

    sel     : in std_logic;
    we      : in std_logic;
    address : in unsigned(ADDR_WIDTH-1 downto 0);
    datain  : in std_logic_vector(DATA_WIDTH-1 downto 0);

    dataout : out std_logic_vector(DATA_WIDTH-1 downto 0));

end ram;

architecture rtl of ram is

  constant ADDR_MAX : natural := 2**ADDR_WIDTH-1;
  
  subtype data_type is std_logic_vector(DATA_WIDTH-1 downto 0);
  
  type ram_type is array(0 to ADDR_MAX) of data_type;

  signal ram_s : ram_type;
  
begin  -- rtl

  tick : process (clk, reset_n)
  begin
    if reset_n = '0' then
      ram_s   <= (others => (others => '0'));
      dataout <= (others => '0');
    elsif rising_edge(clk) then
      if sel = '1' then
        if we = '1' then
          ram_s(to_integer(address)) <= datain;
        else
          dataout <= ram_s(to_integer(address));
        end if;
      end if;
    end if;
  end process;

end rtl;
