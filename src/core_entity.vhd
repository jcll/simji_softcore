-------------------------------------------------------------------------------
-- Title      : Simple (slow) RISC 32 processor with no pipeline
-- Project    : MIPS0
-------------------------------------------------------------------------------
-- File       : core.vhd
-- Author     : Jean-Christophe Le Lann  lelannje@ensta-bretagne.fr
-- Company    :
-- Created    : 2014-03-05
-- Last update: 2018-03-28
-- Platform   :
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-03-05  1.0      jcll    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.type_package.all;

library misc_lib;
use misc_lib.txt_util.all;

entity core is
  generic(CODE_ADDR_WIDTH : natural := 8;
          DATA_ADDR_WIDTH : natural := 8);
  port (
    reset_n       : in  std_logic;
    clk           : in  std_logic;
    boot_address  : in  unsigned(CODE_ADDR_WIDTH-1 downto 0);
    go            : in  std_logic;
    stopped       : out std_logic;
    running       : out std_logic;
    ---------------------------------------------------------------------------
    code_sel      : out std_logic;
    code_we       : out std_logic;
    code_address  : out unsigned(CODE_ADDR_WIDTH-1 downto 0);
    code_in       : in  std_logic_vector(31 downto 0);
    ---------------------------------------------------------------------------
    data_sel      : out std_logic;
    data_we       : out std_logic;
    data_address  : out unsigned(DATA_ADDR_WIDTH-1 downto 0);
    data_out      : out std_logic_vector(31 downto 0);
    data_in       : in  std_logic_vector(31 downto 0);
    leds          : out std_logic_vector(15 downto 0)

    );

end core;
