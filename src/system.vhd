--------------------------------------------------------------------
-- Jean-Christophe Le Lann - ENSTA Bretagne 2018
--------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.type_package.all;

entity system is
  port(
    reset_n       : in  std_logic;
    clk           : in  std_logic;
    go            : in  std_logic;
    mode          : in  running_mode;

    prog_we       : in  std_logic;
    prog_sel      : in  std_logic;
    prog_addr     : in  unsigned(CODE_ADDR_WIDTH-1 downto 0);
    prog_datain   : in  std_logic_vector(31 downto 0);
    prog_dataoout : out std_logic_vector(31 downto 0);

    data_we       : in  std_logic;
    data_sel      : in  std_logic;
    data_addr     : in  unsigned(DATA_ADDR_WIDTH-1 downto 0);
    data_datain   : in  std_logic_vector(31 downto 0);
    data_dataoout : out std_logic_vector(31 downto 0);
    stopped       : out std_logic
    );
end system;

architecture rtl of system is

  signal boot_address      : unsigned(CODE_ADDR_WIDTH-1 downto 0);
  --
  signal core_code_sel          : std_logic;
  signal core_code_we           : std_logic;
  signal core_code_address      : unsigned(CODE_ADDR_WIDTH-1 downto 0);
  signal core_code_in           : std_logic_vector(31 downto 0);
  signal core_code_out          : std_logic_vector(31 downto 0);
  --
  signal core_data_sel          : std_logic;
  signal core_data_we           : std_logic;
  signal core_data_address      : unsigned(DATA_ADDR_WIDTH-1 downto 0);
  signal core_data_in           : std_logic_vector(31 downto 0);
  signal core_data_out          : std_logic_vector(31 downto 0);
  -- sig for mux
  signal code_sel_s        : std_logic;
  signal code_we_s         : std_logic;
  signal code_address_s    : unsigned(CODE_ADDR_WIDTH-1 downto 0);
  signal code_in_s         : std_logic_vector(31 downto 0);
  signal code_out_s        : std_logic_vector(31 downto 0);
  --
  signal data_sel_s          : std_logic;
  signal data_we_s           : std_logic;
  signal data_address_s      : unsigned(DATA_ADDR_WIDTH-1 downto 0);
  signal data_in_s           : std_logic_vector(31 downto 0);
  signal data_out_s          : std_logic_vector(31 downto 0);

begin

  boot_address <= DEFAULT_BOOT_ADDRESS;
  --init         <= '1' when mode = BOOT_MODE else '0';

  core_2 : entity work.core(archi_v0)
    generic map (
      CODE_ADDR_WIDTH => CODE_ADDR_WIDTH,
      DATA_ADDR_WIDTH => DATA_ADDR_WIDTH)
    port map (
      reset_n       => reset_n,
      clk           => clk,
      boot_address  => boot_address,
      go            => go,
      stopped       => stopped,
      code_sel      => core_code_sel,
      code_we       => core_code_we,
      code_address  => core_code_address,
      code_in       => core_code_in,
      data_sel      => core_data_sel,
      data_we       => core_data_we,
      data_address  => core_data_address,
      data_out      => core_data_out,
      data_in       => core_data_in);

  mem_code : entity work.ram(RTL)
    generic map (
      ADDR_WIDTH => CODE_ADDR_WIDTH,
      DATA_WIDTH => 32)
    port map (
      reset_n => reset_n,
      clk     => clk,
      sel     => code_sel_s,
      we      => code_we_s,
      address => code_address_s,
      datain  => code_in_s,
      dataout => code_out_s);

  core_code_in <= code_out_s;

  mem_data : entity work.ram(RTL)
    generic map (
      ADDR_WIDTH => DATA_ADDR_WIDTH,
      DATA_WIDTH => 32)
    port map (
      reset_n => reset_n,
      clk     => clk,
      sel     => data_sel_s,
      we      => data_we_s,
      address => data_address_s,
      datain  => data_in_s,
      dataout => data_out_s);

  core_data_in <= data_out_s;
  -----------------------------------------------------------------------------
  -- MUX to enable access to the instruction RAM from outside
  -----------------------------------------------------------------------------
  mux_prog_mem : process (core_code_address, core_code_sel, core_code_we, mode,
                          prog_addr, prog_datain,prog_sel,prog_we)
  begin
    if mode = PROG_MODE then
      code_address_s <= prog_addr;
      code_sel_s     <= prog_sel;
      code_we_s      <= prog_we;
      code_in_s      <= prog_datain;
    else
      code_address_s <= core_code_address;
      code_sel_s     <= core_code_sel;
      code_we_s      <= core_code_we;
      code_in_s      <= (others=>'0');
    end if;
  end process;

  mux_data_mem : process (core_data_address, core_data_out, core_data_sel,
                          core_data_we, data_addr, data_datain, mode)
  begin
    if mode = PROG_MODE then
      data_address_s <= data_addr;
      data_sel_s     <= '1';
      data_we_s      <= '1';
      data_in_s      <= data_datain;
    else
      data_address_s <= core_data_address;
      data_sel_s     <= core_data_sel;
      data_we_s      <= core_data_we;
      data_in_s      <= core_data_out;
    end if;
  end process;

end rtl;
