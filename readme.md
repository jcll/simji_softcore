# Simji softcore

Simji softcore is a simple embeddable core featuring a 32-bits RISC instruction set. To use it, it needs a surounding environment made of instruction memory, data memory, and everything needed to write and read back programs and data in/to the memories.

Companion projet Simji SoC offers such an environment, as well
as :
* VHDL simulation scripts
* VHDL synthesis scripts for Xilinx FPGA (Artix7 on Digilent NexysA7)
* example assembly programs. Of course some of them even make leds blink !

Please jump to Simji SoC project.
